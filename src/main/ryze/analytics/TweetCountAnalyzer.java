package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

import ryze.query.Tweet;

public class TweetCountAnalyzer  implements Analyzer<Tweet, Integer>{

/**
 * Simple analyzer that counts number of tweets
 */
	@Override
	public List<Integer> analyze(List<Tweet> tweets) {
		LinkedList<Integer> res = new LinkedList<>();
		res.add(tweets.size());
		return res;
	}

}
