package ryze.web;

import org.rythmengine.Rythm;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Method;
import fi.iki.elonen.NanoHTTPD.Response;

/** Simple hello test page *
 * 
 * @author whwong
 *
 */
public class HelloHandler implements Handler {

	@Override
	public Response handle(Method m, String uri, IHTTPSession session) {
        String msg = Rythm.render("hello.html", session.getParms().get("username"));

        return NanoHTTPD.newFixedLengthResponse(msg);
	}

}
