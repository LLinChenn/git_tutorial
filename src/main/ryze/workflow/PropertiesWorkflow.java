package ryze.workflow;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import ryze.analytics.Analyzer;
import ryze.billing.Biller;
import ryze.query.Query;

/**
 * Loads a workflow from the classes defined in a properties file.
 * 
 * @author whwong
 *
 * @param <A> Type of data produced by the Query module
 * @param <B> Type of data produced by the Analyzer module
 * @param <C> Type of data produced by the Biller module
 */
public class PropertiesWorkflow<A,B,C> implements Workflow<A,B,C>{
	
	private Query<A> query;
	private Analyzer<A,B> analyzer;
	private Biller<B,C> biller;
	
	private final String[] propertyNames = {"query", "analyzer", "biller"};
	
	@SuppressWarnings("unchecked")
	public PropertiesWorkflow(Properties props) 
			throws 
			PropertyMissingException, ClassNotFoundException, InstantiationException, 
			IllegalAccessException, TypeMismatchException
	{
		
		LinkedList<String> missingProps = new LinkedList<>();

		for(String prop: propertyNames){
			String p = props.getProperty(prop);
			if(p == null){
				missingProps.add(p);
			}
		}
		
		if(!missingProps.isEmpty()){
			throw new PropertyMissingException("Missing properties: " + String.join(",", missingProps));
		}

		String queryClazz = props.getProperty("query");
		String analyzerClazz = props.getProperty("analyzer"); 
		String billerClazz = props.getProperty("biller");
		
		query = loadClass(queryClazz, Query.class);
		analyzer = loadClass(analyzerClazz, Analyzer.class);
		biller = loadClass(billerClazz, Biller.class);
		
		validateWorkflow(query, analyzer, biller);
	}
	
	@SuppressWarnings("rawtypes")
	private boolean validateWorkflow(Query<A> query, Analyzer<A,B> analyzer, Biller<B,C> biller)
		throws TypeMismatchException, ClassNotFoundException{
		Class<? extends Query> qClazz = query.getClass();
		String queryOutType = "";
		String[] qOut = getInOutTypes(qClazz, "query", new Class[]{String[].class});
		queryOutType = qOut[1];
		System.out.printf("query: () -> List<%s>%n", queryOutType);

		Class<? extends Analyzer> analyClazz = analyzer.getClass();
		String analyzerInType = "", analyzerOutType = "";
		String[] analyInOut = getInOutTypes(analyClazz, "analyze", new Class[]{List.class});
		analyzerInType = analyInOut[0]; analyzerOutType = analyInOut[1];
		System.out.printf("analyzer: List<%s> -> List<%s>%n", analyzerInType, analyzerOutType);
		
		
		Class<? extends Biller> billClazz = biller.getClass();
		String billerInType = "", billerOutType = "";
		String[] billInOut = getInOutTypes(billClazz, "bill", new Class[]{List.class});
		billerInType = billInOut[0]; billerOutType = billInOut[1];
		System.out.printf("biller: List<%s> -> List<%s>%n", billerInType, billerOutType);
		
		//verify that the workflow types are compatible
		Class<?> qOutClazz = Class.forName(queryOutType);
		Class<?> analyInClazz = Class.forName(analyzerInType);
		
		if(!analyInClazz.isAssignableFrom(qOutClazz)){
			throw new TypeMismatchException(qOutClazz, analyInClazz);
		}
		
		Class<?> billerInClazz = Class.forName(billerInType);
		Class<?> analyOutClazz = Class.forName(analyzerOutType);
		if(!billerInClazz.isAssignableFrom(analyOutClazz)){
			throw new TypeMismatchException(analyOutClazz, billerInClazz);
		}

		return true;

	}
	
	/** 
	 * Utility method to extract actual type parameters. 
	 * Limited to only the Query, Analyzer and Biller classes.
	 * Brittle - will break if their interfaces change.
	 * i.e. assumes methods only have at most 1 argument. 
	 * i.e.2. type parameters can be resolved to classes in one step e.g. ? extends Number; ? extends T is not supported 
	 * i.e.3. not strictly but assumes the argument and return type to be List<T> or a collection with a single type parameter; Map<K,V> don't work 
	 * @param clazz
	 * @param method
	 * @param params
	 * @return
	 */
	private String[] getInOutTypes(Class<?> clazz, String method, Class[] params){
		
		String[] res = new String[2];
		try {
			Method m = clazz.getMethod(method, params);
			
			Type[] paramTypes = m.getGenericParameterTypes();
			if(paramTypes.length > 0 && paramTypes[0] instanceof ParameterizedType){
				ParameterizedType ppm = (ParameterizedType)paramTypes[0];
				Type firstTypeArg = ppm.getActualTypeArguments()[0];
				if(firstTypeArg instanceof WildcardType){
					WildcardType wild = (WildcardType)firstTypeArg;
					Type[] wildUpper = wild.getUpperBounds();
					res[0] = wildUpper[0].getTypeName();
				}else{
					res[0] = firstTypeArg.getTypeName();
				}
			}	
			
			ParameterizedType pm = ((ParameterizedType) m.getGenericReturnType());
			res[1] = pm.getActualTypeArguments()[0].getTypeName();

		} catch (NoSuchMethodException e) {
			//should never happen but just in case
			e.printStackTrace();
		} catch (SecurityException e) {
			//should never happen but just in case
			e.printStackTrace();
		}
		return res;
	}

	private <T> T loadClass(String clazzName, Class<T> type) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		Class<?> clazz = Class.forName(clazzName);
		return type.cast(clazz.newInstance());
	}
	
	@Override
	public Query<A> getQuery() {
		return query;
	}

	@Override
	public Analyzer<A, B> getAnalyzer() {
		return analyzer;
	}

	@Override
	public Biller<B,C> getBiller() {
		return biller;
	}

	public static void main(String[] args) throws Exception{
		Properties p = new Properties();
		p.setProperty("query", "ryze.query.TwitterQuery");
		p.setProperty("analyzer", "ryze.analytics.CountAnalyzer");
		p.setProperty("biller", "ryze.billing.CountBiller");
		
		PropertiesWorkflow<String,Double, Double> pwf = new PropertiesWorkflow<>(p);
		
	}
}
