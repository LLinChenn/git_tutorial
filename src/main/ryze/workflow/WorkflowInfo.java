package ryze.workflow;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@SuppressWarnings("rawtypes")
public class WorkflowInfo {

	public String name;
	public Date lastModified;
	public Workflow workflow;

	public WorkflowInfo(String wfName, Date lastModified, Workflow wf) {
		this.name = wfName;
		this.lastModified = lastModified;
		this.workflow = wf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Workflow getWorkflow() {
		return workflow;
	}

//	public void setWorkflow(Workflow workflow) {
//		this.workflow = workflow;
//	}

	public String getLastModified() {
		return LocalDateTime.ofInstant(lastModified.toInstant(), ZoneId.systemDefault()).toString();
	}
	
	public String getQueryName(){
		return workflow.getQuery().getClass().getSimpleName();
	}
	
	public String getAnalyzerName(){
		return workflow.getAnalyzer().getClass().getSimpleName();
	}

	public String getBillerName(){
		return workflow.getBiller().getClass().getSimpleName();
	}
}
