package ryze.ui;

import java.util.List;

import ryze.analytics.Analyzer;
import ryze.analytics.CountAnalyzer;
import ryze.billing.Biller;
import ryze.billing.FlatRateBiller;
import ryze.query.TwitterTextQuery;

public class RyzeCommandLineUI {

	public static void main(String[] args) throws Exception{
		TwitterTextQuery tq = new TwitterTextQuery(200);
		
		Analyzer<String, Integer> ca = new CountAnalyzer();
		
		Biller<Number, Double> bi = new FlatRateBiller(0.01);
		System.out.printf("Obtaining tweets...%n");
		
		List<String> tweets = tq.query(args);
		

		List<Integer> counts = ca.analyze(tweets);
		
		List<Double> bill = bi.bill(counts);
		
		System.out.printf("Charge of $%.2f based on %d tweets containing the keywords [%s]%n",
				bill.get(0),
				counts.get(0),
				String.join(", ", args)
				);
		
		
		for(String t: tweets){
			System.out.println(t);
		}
		
		
		
		
	}
}
