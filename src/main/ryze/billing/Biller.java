package ryze.billing;

import java.util.List;

public interface Biller<A,B> {

//	List<B> bill(List<? extends A> analysis); //list input should actually be parameterised by A as such 
											  // but for the limitation of the Ryze type checking mechanism
	List<B> bill(List<? extends A> analysis);
}
