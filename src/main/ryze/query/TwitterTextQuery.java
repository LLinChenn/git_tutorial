package ryze.query;

import java.util.LinkedList;
import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TwitterTextQuery implements ryze.query.Query<String>{

	private Twitter twitter;
	private int maxTweets = 200;
	
	public int getMaxTweets() {
		return maxTweets;
	}

	public void setMaxTweets(int maxTweets) {
		this.maxTweets = maxTweets;
	}

	public TwitterTextQuery(){
		twitter = new TwitterFactory().getInstance();
	}
	
	public TwitterTextQuery(int maxTweets){
		this();
		this.maxTweets = maxTweets;
	}

	@Override
	public List<String> query(String... keywords) {
		//TODO process keywords to force them into basic words
		//TODO return List<Tweet> rather than just msgs
		String queryStr = String.join(" ", keywords);
		Query query = new Query(queryStr);
		query.count(maxTweets);
		
		QueryResult result;
		List<String> txtTweets = new LinkedList<>();
		try{
			do {
				result = twitter.search(query);
				List<Status> tweets = result.getTweets();
				for (Status tweet : tweets) {
					txtTweets.add(tweet.getText());
//					System.out.println("@" + tweet.getUser().getScreenName() + " - " + tweet.getText());
				}
			} while (txtTweets.size() < maxTweets && (query = result.nextQuery()) != null);
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to search tweets: " + te.getMessage());
		}
		
		return txtTweets;
	}


}
