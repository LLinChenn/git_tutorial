package ryze.query;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

public class FullTwitterQuery implements ryze.query.Query<Tweet>{

	private static Logger LOG = Logger.getLogger(FullTwitterQuery.class.getName());

	private Twitter twitter;
	private int maxTweets = 200;
	
	public int getMaxTweets() {
		return maxTweets;
	}

	public void setMaxTweets(int maxTweets) {
		this.maxTweets = maxTweets;
	}

	public FullTwitterQuery(){
		twitter = new TwitterFactory().getInstance();
	}
	
	public FullTwitterQuery(int maxTweets){
		this();
		this.maxTweets = maxTweets;
	}

	@Override
	public List<Tweet> query(String... keywords) {
		//TODO process keywords to force them into basic words
		String queryStr = String.join(" ", keywords);
		Query query = new Query(queryStr);
		query.count(maxTweets);
		
		QueryResult result;
		List<Tweet> fullTweets = new LinkedList<>();
		try{
			do {
				result = twitter.search(query);
				List<Status> tweets = result.getTweets();
				for (Status tweet : tweets) {
					Tweet t = new Tweet(tweet);
					fullTweets.add(t);
				}
			} while (fullTweets.size() < maxTweets && (query = result.nextQuery()) != null);
		} catch (TwitterException te) {
			LOG.log(Level.SEVERE, "Failed to search tweets", te);
		}
		
		return fullTweets;
	}


}
